﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Mirror;

public class ChangeSprite : NetworkBehaviour
{
    //public SpriteRenderer spriteR;
    public Sprite[] sprites;
    public uint playerId;
    public Sprite mySprite;
    //NetworkIdentity.spawned[theNetworkId]

    // private Object[] textures;
    // Start is called before the first frame update
    void Start()
    {
        playerId = this.netId;
        LoadSprites();
        //spriteR = gameObject.GetComponent<SpriteRenderer>();
        //Sprite[] sprites = Resources.LoadAll("Sprites", typeof(Sprite));
        Sprite mySprite = GetComponent<SpriteRenderer>().sprite;
        //textures = Resources.LoadAll("Textures", typeof(Texture2D));
        if (isServer)
        {
            if (playerId == 0)
            {
                mySprite = sprites[1];
            }
            else if (playerId == 1)
            {
                mySprite = sprites[1];
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //VIA @YoucefB ON UNITY ANSWERS
    //public Sprite[] Icons; // icons array
    void LoadSprites()
    {
        object[] loadedSprites = Resources.LoadAll("Sprites", typeof(Sprite));
        sprites = new Sprite[loadedSprites.Length];
        //this
        for (int x = 0; x < loadedSprites.Length; x++)
        {
            sprites[x] = (Sprite)loadedSprites[x];
        }
        //or this
        //loadedIcons.CopyTo (Icons,0);

    }
}
