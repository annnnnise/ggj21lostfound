﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class MovePlayer : NetworkBehaviour
{
    private Rigidbody2D rb2D;
    
    
    // Start is called before the first frame update
    void Start()
    {
        rb2D = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }
        if (Input.GetAxis("Vertical") != 0)
        {
            VMovement();
        }
        else if(Input.GetAxis("Horizontal") != 0)
        {
            HMovement();
        }
        else
        {
            StopMovement();
        }
    }

    void HMovement()
    {
        /*float vertMove = Input.GetAxis("Vertical")*1f;
        float horiMove = Input.GetAxis("Horizontal")* 1f;
        if(vertMove > 5f)
        {
            vertMove = 5f;
        }
        if (horiMove > 5f)
        {
            horiMove = 5f;
        }
        rb2D.velocity = rb2D.velocity + new Vector2(horiMove, vertMove);*/
        if(Input.GetAxis("Horizontal") > 0)
        {
            rb2D.velocity = new Vector2(20.0f, 0);
        }
        else if(Input.GetAxis("Horizontal") < 0)
        {
            rb2D.velocity = new Vector2(-20.0f, 0);
        }

    }

    void VMovement()
    {
        /*float vertMove = Input.GetAxis("Vertical")*1f;
        float horiMove = Input.GetAxis("Horizontal")* 1f;
        if(vertMove > 5f)
        {
            vertMove = 5f;
        }
        if (horiMove > 5f)
        {
            horiMove = 5f;
        }
        rb2D.velocity = rb2D.velocity + new Vector2(horiMove, vertMove);*/
        if (Input.GetAxis("Vertical") > 0)
        {
            rb2D.velocity = new Vector2(0, 20.0f);
        }
        else if (Input.GetAxis("Vertical") < 0)
        {
            rb2D.velocity = new Vector2(0, -20.0f);
        }
    }

    void StopMovement()
    {
        rb2D.velocity = new Vector2(0, 0);
    }
}
