﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : MonoBehaviour
{
    private Rigidbody2D rb2D;
    private float enemyMove = 0.1f;
    public bool horizontalMover;
    //public float horiMove = 0.3f;
    // Start is called before the first frame update
    void Start()
    {
        rb2D = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        enemyMoves();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag.Equals("Environment"))
        {
            enemyMove = -(enemyMove);
        }
    }

    void enemyMoves()
    {
        if (horizontalMover)
        {
            rb2D.position = rb2D.position + new Vector2(enemyMove, 0);
        }
        else
        {
            rb2D.position = rb2D.position + new Vector2(0, enemyMove);
        }
    }
}
